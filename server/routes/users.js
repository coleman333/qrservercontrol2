const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/users');


// router.get('/', userCtrl.fetch);
// router.post('/registration', userCtrl.upload.single('avatar'), userCtrl.registration);
// router.post('/login', userCtrl.login);
// router.get('/logout', userCtrl.logout);
// router.post('/isEmail', userCtrl.isEmail);
// console.log("this is server 1234")
//
console.log('this is server side routes');

router.post('/push-token', userCtrl.createPushToken);
router.post('/save-to-base-edited-result', userCtrl.saveToBaseEditedResult);
router.post('/get-code-response', userCtrl.getTheDataFromCodeNumber);
router.post('/test-tesseract-ocr', userCtrl.testTesseract);
router.post('/test-tesseract-ocr-regex', userCtrl.parseText);
router.post('/get-all-items', userCtrl.getAllItems);

// router.post('/text-pre-engraving', userCtrl.textPreEngraving);
router.post('/login', userCtrl.logVisits , userCtrl.login);

router.put('/registration', userCtrl.registration);
router.put('/check-name', userCtrl.checkName);
router.get('/product/:product',userCtrl.getProduct);
router.put('/create-product',userCtrl.createProduct);
router.post('/send-push-notifications',userCtrl.sendPushNotifications);
router.post('/send-push-notifications2',userCtrl.sendPushNotifications2);

module.exports = router;


