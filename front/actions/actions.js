import {createActionAsync} from 'redux-act-async';
import axios from 'axios';

export default {
    testTesseract: createActionAsync('TEST_TESSERACT', () => {
        console.log('this is action side')
        return axios({
            method: 'post',
            // url: `https://localhost:4433/api/auth/test-tesseract-ocr`,
            url: `http://localhost:3003/api/users/test-tesseract-ocr`,
        });
    }, {rethrow: true}),
    readText: createActionAsync('TEST_TESSERACT_REGEX', () => {
        console.log('this is action side')
        return axios({
            method: 'post',
            // url: `https://localhost:4433/api/auth/test-tesseract-ocr`,
            url: `http://localhost:3003/api/users/test-tesseract-ocr-regex`,
        });
    }, {rethrow: true})
}