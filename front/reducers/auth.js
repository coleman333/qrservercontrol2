import {createReducer} from 'redux-act';
import cloneDeep from 'lodash/cloneDeep';
import actions from '../actions/actions';

const initialState = {
	processing: 0,
	error: {},
	user: {},
};

export default createReducer({
	// [authActions.registration.request]: (state, payload) => {
	// 	const newState = cloneDeep(state);
	//
	// 	return newState;
	// },
	// [authActions.registration.ok]: (state, payload) => {
	// 	const newState = cloneDeep(state);
	//
	// 	return newState;
	// },
	// [authActions.registration.error]: (state, payload) => {
	// 	const newState = cloneDeep(state);
	//
	// 	return newState;

	[actions.testTesseract.request]: (state, payload) => {
		const newState = cloneDeep(state);

		return newState;
	},
	[actions.testTesseract.ok]: (state, payload) => {
        const newState = cloneDeep(state);
        newState.text = payload.response.data;
        console.log('this is from reducer', newState.text);
        return newState;
	},
	[actions.testTesseract.error]: (state, payload) => {
		const newState = cloneDeep(state);

		return newState;
	},

}, initialState);
