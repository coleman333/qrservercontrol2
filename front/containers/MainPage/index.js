import "app/assets/styles/main.scss";
// import './styles.scss';
import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import {browserHistory} from 'react-router';
import actions from '../../actions/actions';
// import  from 'react-router';


class MainPage extends Component {
	static propTypes = {};

	state = {};

	constructor(props) {
		super(props);
	}

	authFB(){
		// this.props.authFacebook
	}
    testTesseract(){
		console.log('this is test method')
        this.props.actions.testTesseract();
	}
    testTesseractRegex(){
		console.log('this is test method')
        this.props.actions.readText();
	}


	render() {
console.log(111)
		const {children,} = this.props;

		return <div className="">
			MAIN page1
			<button onClick={this.authFB.bind(this)}>facebook</button>
			<button onClick={this.testTesseract.bind(this)}>test tesseract</button>
			<button onClick={this.testTesseractRegex.bind(this)}>test tesseract regex</button>
			<a href="https://localhost:4433/api/auth/facebook">ClickMe</a><br/>
			{/*<a href="https://localhost:4433/api/auth/test-tesseract-ocr">test tesseract</a>*/}
			{children}
		</div>
	}

}

const mapStateToProps = (state) => {
	return {
        text: state.auth.text
    }
};

const mapDispatchToProps = (dispatch) => {
	return {
        actions: bindActionCreators(actions, dispatch),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
