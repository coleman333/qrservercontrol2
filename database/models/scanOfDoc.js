const mongoose = require('mongoose');
const Schema = mongoose.Schema;
autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.createConnection("mongodb://localhost/myDatabase");

autoIncrement.initialize(connection);

const scanOfDocSchema = new Schema({
    incomeOrder: {type: String, required: false},
    providerCode: {type: String, required: false},
    guestNumber: {type: String, required: false},
    buyingOrder: {type: String, required: false},
    serialNumber:{type: String, required: false},
    item:{type: String, required: false}

});

module.exports = mongoose.model('ScanOfDoc', scanOfDocSchema);

