const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logVisitingSchema = new Schema({
   name: {type:String,required:true},
   date: {type:Date,required: true},
   action: {type: String, required: false},
   staffName: {type: String, required: false},
   amount: {type: String, required: false},

});

module.exports = mongoose.model('LogVisiting',logVisitingSchema);